// Створіть 3 змінних ( x = 6, y = 14, z = 4 ) Виконайте та відобразіть результат наступних операцій для цих змінних у коментарях опишіть чому програма так порахувала:
// x += y - x++ * z;
// z = -x - y * 5;
// y / = x + 5% z;
// z - x + + + y * 5;
// x = y - x ++ * z;

let x = 6;
let y = 14;
let z = 4;

let firstTask = x += y - x++ * z;
/*
1) x * z = 24. Precedence of "++" is 16 but it will be done after the action "*" (Precedence is 13). 
2) x++ = 7. 
3) y - action 1 = -10. Precedence of "-" is 12
4) x (from the action 2) += action 3 = 7+7-10 = -4. Precedence of "+=" is 2
*/


let secondTask = z = -x - y * 5;
/*
1) y * 5 = 70. Precedence of "*" is 13
2) -x - action 1 = -76. Precedence of "-" is 12
3) z = action 2 = -76. Precedence of "=" is 2
*/


let thirdTask = y /= x + 5% z;
/*
1) 5%z = 1. Precedence of "%" is 13
2) x + action 1 = 7. Precedence of "+" is 12
3) y /= action 2 = 14 / 7 = 2. Precedence of "/=" is 2
*/

let forthTask = z - x + + + y * 5;
/*
1) y * 5 = 70. Precedence of "*" is 13
2) z - x = -2. Precedence of "-" is 12. The calculation is sperformed from left to right
3) action 2 +++ action 1 = 68. Precedence of "+" is 12. But this action isss performed after the action 2 due to the calculation order.
*/


let fifthTask = x = y - x ++ * z;
/*
1) x++ has the precedence 13 but it will not work out in this situation as it is post-prefix increment and we have no calcilations with x in this task.
2) x * z = 24. Precedence of "*" is 13. 
3) y - action 2  = -10. Precedence of "-" is 12. 
4) x = action 3 = -10. Precedence of "=" is 2
*/


